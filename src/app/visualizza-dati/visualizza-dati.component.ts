import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { Film } from '../service-get/film';
 
import { DataGetService } from '../service-get/data-get.service';
@Component({
  selector: 'app-visualizza-dati',
  templateUrl: './visualizza-dati.component.html',
  styleUrls: ['./visualizza-dati.component.css'],
  providers: [DataGetService],
  animations: [
    trigger('fade', [

      transition('void => *', [
        style({ opacity: 0 }),
        animate(700)
      ])
    ])
  ]
})
export class VisualizzaDatiComponent implements OnInit {

  datiComponente: Film[];
  cliccato = false;
  titoloOriginale = true;
  
  constructor(private datiRicevuti: DataGetService) { }

  ngOnInit() {
    this.caricaDati();
    
  }

  caricaDati() {
    this.datiRicevuti.getDatiAPI().subscribe(dato => {
      this.datiComponente = dato;
      console.log(this.datiComponente);
    });
  }

  ordinaDati() {

    this.cliccato = !this.cliccato;

    let datiOrdinati = this.datiComponente.sort((a, b) => {
      return b.valutazione - a.valutazione;
    });

    if (this.cliccato) {
      this.datiComponente = datiOrdinati;
    } else {
      this.datiComponente = datiOrdinati.reverse();
    }

    console.log(this.cliccato);
  }


  convertiDurata(durataMinuti:any){
    let h:any = Math.floor(durataMinuti / 60);
    let m:any = durataMinuti % 60;
    h = h < 10 ? '' + h : h;
    m = m < 10 ? '0' + m : m;
    return h+":"+m;

  }




}
