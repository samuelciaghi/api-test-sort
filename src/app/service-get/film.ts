export interface Film {
   titolo:string,
   anno:number,
   valutazione:number,
   durata:number 
}
