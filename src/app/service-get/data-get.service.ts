import { Injectable } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Film } from './film';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators'

@Injectable()
export class DataGetService {

  constructor(private miohttp: HttpClient) { }


  getDatiAPI(): Observable<Film[]> {
    return this.miohttp.get<Film[]>('https://ghibliapi.herokuapp.com/films')
      .pipe(
        //utilizzo pipe e map per creare un array di oggetti selezionando solo 
        //i dati dell'api necessari, rispettando il tipo Film[]
        //che riceverò dopo il subscribe
        map((filmRicevuti: any) => {
          return filmRicevuti.map(film =>
          ({
            titolo: film.title,
            titoloOriginale: film.original_title,
            anno: film.release_date,
            valutazione: film.rt_score,
            durata: film.running_time

          }))
        })

      )
  }

}