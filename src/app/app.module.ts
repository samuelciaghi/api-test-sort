import { BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';

import { HttpClientModule } from '@angular/common/http';
import { VisualizzaDatiComponent } from './visualizza-dati/visualizza-dati.component';
@NgModule({
  imports:      [ BrowserModule, FormsModule,HttpClientModule, BrowserAnimationsModule ],
  declarations: [ AppComponent, VisualizzaDatiComponent ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
